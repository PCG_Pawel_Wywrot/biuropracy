﻿using BiuroPracy.BusinessLogic.Communication;
using BiuroPracy.BusinessLogic.ModelDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiuroPracy.BusinessLogic.Api.Interface
{
    public interface IBiuroPracyApi
    {
        void TestNHibernate();
        LsitIdNamePairServiceResponse GetProfessions();
        LsitIdNamePairServiceResponse GetContract();
        ServiceResponder AddEmployee(EmployeeDto emp);

    }
}

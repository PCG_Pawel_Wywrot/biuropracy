﻿using BiuroPracy.BusinessLogic.Api.Interface;
using BiuroPracy.BusinessLogic.NHibernate;
using BiuroPracy.Domain;
using BiuroPracy.Domain.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BiuroPracy.BusinessLogic.Communication;
using NHibernate.Linq;
using BiuroPracy.BusinessLogic.ModelDTO;
using BiuroPracy.BusinessLogic.Extensions;
using BiuroPracy.BusinessLogic.Logic;

namespace BiuroPracy.BusinessLogic.Api
{
   public  class BiuroPracyApi : BaseApi, IBiuroPracyApi
    {
        public ServiceResponder AddEmployee(EmployeeDto employee)
        {
            using (var session = NHibernateBase.Session)
            {
                using (var transaction = session.BeginTransaction())
                {
                    try
                    {
                        var employeeAdd = employee.ToEmployee();
                        //session.Save(employeeAdd.ContractID);
                        //session.Save(employeeAdd.ProfessionID);
                        session.Save(employeeAdd);
                        transaction.Commit();
                        var email = new EmailManager();
                        email.SendEmail("Dodano pracownika", "haslo " + employeeAdd.Password, employeeAdd.Email);

                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        return new ServiceResponder()
                        {
                            Errors = e.StackTrace + " " + e.Message,
                            Success = false
                        };
                    }
                }
    
            }
            return new ServiceResponder();
        }
        public void TestNHibernate()
        {
            try
            {
                using (var session = NHibernateBase.Session )
                {
                    using (var transaction  = session.BeginTransaction())
                    {
                        var profession = session.Get<Profession>(1);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public LsitIdNamePairServiceResponse GetProfessions()
        {
            try
            {
                using (var session = NHibernateBase.Session)
                {
                    var professions = session.Query<Profession>().Select(x => new IdPairName
                    {
                        ID = x.Id,
                        Name = x.Name
                    }).ToList();

                    return new LsitIdNamePairServiceResponse()
                    {
                        Data = professions
                    };
                }
            }
            catch (Exception e)
            {
                return new LsitIdNamePairServiceResponse()
                {
                    Errors = e.StackTrace + " " + e.Message,
                    Success = false
                };
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public LsitIdNamePairServiceResponse GetContract()
        {
            try
            {
                using (var session = NHibernateBase.Session)
                {
                    var contracts = session.Query<Contract>().Select(x => new IdPairName
                    {
                        ID = x.Id,
                        Name = x.Name
                    }).ToList();

                    return new LsitIdNamePairServiceResponse()
                    {
                        Data = contracts
                    };
                }
            }
            catch (Exception e)
            {
                return new LsitIdNamePairServiceResponse()
                {
                    Errors = e.StackTrace + " " + e.Message,
                    Success = false
                };
            }
        }

    }
}

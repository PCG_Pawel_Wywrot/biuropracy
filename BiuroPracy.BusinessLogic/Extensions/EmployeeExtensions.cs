﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BiuroPracy.Domain.Domain;
using BiuroPracy.BusinessLogic.ModelDTO;

namespace BiuroPracy.BusinessLogic.Extensions
{
    public static class EmployeeExtensions
    {
        public static Employee ToEmployee(this EmployeeDto dto)
        {
            if (dto == null)
                return null;

            return new Employee
            {
                Id = 3,
                Name = dto.Name,
                Email = dto.Email,
                Surname = dto.Surname,
                Password = dto.Password,
                DateOfBirth = dto.DateOfBirth,
                ProfessionID = dto.ProfessionId > 0 ? new Profession { Id = dto.ProfessionId } : null,
                ContractID = dto.ContractId > 0 ? new Contract { Id = dto.ContractId } : null,
                LocationID = new Location()
                {
                    //Id = 1,
                    CityID = new City { Id = 3 }, //dto.CityId > 0 ? new City { Id = dto.CityId } : null,
                    CountryID = new Country { Id = 3 }, //dto.CountryId > 0 ? new Country { Id = dto.CountryId } : null,
                    Street = dto.Street,
                    Postalcode = dto.PostalCode
                }

            };
        }
    }
}

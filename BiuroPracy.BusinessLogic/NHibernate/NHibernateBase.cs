﻿using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiuroPracy.BusinessLogic.NHibernate
{
    class NHibernateBase
    {
        private static Configuration configuration { get; set; }
        private static ISession session { get; set; }
        private static IStatelessSession statelessSession{ get; set; }



        protected static ISessionFactory sessionFactory { get; set; }
        public static ISession Session => session = sessionFactory.OpenSession();
        public static IStatelessSession StatelessSession => statelessSession ?? (statelessSession = sessionFactory.OpenStatelessSession());


        private static Configuration ConfigurateNHibernate()
        {
            configuration = new Configuration();
            configuration.Configure();
            return configuration;
        }

        public void Initialize()
        {
            configuration = ConfigurateNHibernate();
            sessionFactory = configuration.BuildSessionFactory();
            new SchemaExport(configuration);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiuroPracy.BusinessLogic.ModelDTO
{
    public class IdPairName
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}

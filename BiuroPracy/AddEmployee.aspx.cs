﻿using BiuroPracy.BusinessLogic.Api.Interface;
using BiuroPracy.BusinessLogic.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BiuroPracy.BusinessLogic.ModelDTO;

namespace BiuroPracy
{
    public partial class AddEmployee : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnTestNHib_Click (object serder, EventArgs e)
        {
            IBiuroPracyApi api = new BiuroPracyApi();
            api.TestNHibernate();
        }
        private EmployeeDto GetEmployeeDto()
        {
            return new EmployeeDto()
            {
                CityId = crtlAddPlace.CityId,
                CountryId = crtlAddPlace.CountryId,
                PostalCode = crtlAddPlace.PostalCode,
                Street = crtlAddPlace.Street,
                Email = crtlAddEmployee.Email,
                Password = crtlAddEmployee.Password,
                Name = crtlAddEmployee.Name,
                Surname = crtlAddEmployee.Surname,
                DateOfBirth = crtlAddEmployee.DateOfBirth,
                ProfessionId = crtlAddEmployee.ProfessionId,
                ContractId = crtlAddEmployee.ContractId
            };
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            var api = new BiuroPracyApi();
            var result = api.AddEmployee(GetEmployeeDto());
            if (!result.Success)
            {
                Trace.Warn(result.Errors);
                lblInfo.Text = "blad  dodawania";
                panelInfo.CssClass = "alert alert-danger";

            }
            else
            {
                crtlAddEmployee.ClearControls();
                crtlAddPlace.ClearControls();
                panelInfo.CssClass = "alert alert-success";
                lblInfo.Text = "Dodano pracownika";
            }
        }
    }
}
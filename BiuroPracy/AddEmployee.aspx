﻿<%@ Page Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true" CodeBehind="AddEmployee.aspx.cs" Inherits="BiuroPracy.AddEmployee" %>
<%@ Register TagPrefix="employee" TagName="AddEmployeeControl" Src="~/Controls/AddEmployeeControl.ascx" %>
<%@ Register TagPrefix="place" TagName="AddPlace" Src="~/Controls/PlaceOfResidenceControl.ascx" %>

<%--<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
</body>
</html>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server"></asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentBody" runat="server">
    <h4>Dodawanie pracowników</h4>
    <asp:Panel ID="panelInfo" runat="server">
        <asp:Label ID="lblInfo" runat="server"></asp:Label>
    </asp:Panel>
    <div class="bodyForm">
          <employee:AddEmployeeControl ID="crtlAddEmployee" runat="server" />
          <place:AddPlace ID="crtlAddPlace" runat="server" />

            <div class="form-group">
             <asp:Button ID="btnSave" CssClass="btn-primary" runat="server" Text="ZAPISZ" OnClick="btnSave_Click" />
           </div>

        <div>
            <asp:Button ID="btnNHibernateTest" runat="server" Text="Test NHib" OnClick="btnTestNHib_Click" />
        </div>
 </div>

    
            
 
</asp:Content>

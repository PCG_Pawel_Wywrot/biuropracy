﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddEmployeeControl.ascx.cs" Inherits="BiuroPracy.Controls.AddEmployeeControl" %>
<div class="form-group">

    <label class="control-label col-sm-2">E-mail: </label>
    <div class="col-sm-10">
        <asp:TextBox ID="txtEmail" class="form-control" runat="server"></asp:TextBox>
       <%-- <asp:RequiredFieldValidator ID="fvEmail" ControlToValidate="txtEmail" runat="server" ErrorMessage="Wymagane pole!!!"></asp:RequiredFieldValidator>--%>
    </div>
</div>

<div class="form-group">

    <label class="control-label col-sm-2">Password: </label>
    <div class="col-sm-10">
        <asp:TextBox ID="txtPassword" class="form-control" runat="server"></asp:TextBox>
    </div>
</div>

<div class="form-group">

    <label class="control-label col-sm-2">Imie:  </label>
    <div class="col-sm-10">
        <asp:TextBox ID="txtSurName" class="form-control" runat="server"></asp:TextBox>
    </div>
</div>
<div class="form-group">

    <label class="control-label col-sm-2">Nazwisko:  </label>
    <div class="col-sm-10">
        <asp:TextBox ID="txtName" class="form-control" runat="server"></asp:TextBox>
    </div>
</div>
<div class="form-group">

    <label class="control-label col-sm-2">Data urodzenia: </label>
    <div class="col-sm-10">
        <asp:Calendar ID="calendarDateOdBirth" runat="server"></asp:Calendar>
    </div>
</div>

<div class="form-group">

    <label class="control-label col-sm-2">Zawód: </label>
    <div class="col-sm-10">
        <asp:DropDownList ID="ddlProffesion" runat="server"></asp:DropDownList>
    </div>
</div>

<div class="form-group">

    <label class="control-label col-sm-2">Umowa o pracę: </label>
    <div class="col-sm-10">
       <asp:DropDownList ID="ddlContractOfEmployment" runat="server"></asp:DropDownList>
    </div>
</div>
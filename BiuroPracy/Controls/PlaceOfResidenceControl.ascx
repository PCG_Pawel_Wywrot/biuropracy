﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PlaceOfResidenceControl.ascx.cs" Inherits="BiuroPracy.Controls.PlaceOfResidenceControl" %>

<div class="form-group">

    <label class="control-label col-sm-2">Ulica: </label>
    <div class="col-sm-10">
        <asp:textbox ID="txtStreet" runat="server"></asp:textbox>
        <asp:RequiredFieldValidator ControlToValidate ="txtStreet" ID="RequiredFieldValidator1" runat="server" ErrorMessage="Pole wymagane!!!"></asp:RequiredFieldValidator>
    </div>
</div>

<div class="form-group">

    <label class="control-label col-sm-2">Kod pocztowy: : </label>
    <div class="col-sm-10">
        <asp:textbox ID="txtZipCode" runat="server"></asp:textbox>
        <%--<asp:RequiredFieldValidator ControlToValidate ="txtZipCode" ID="RequiredFieldValidator2" runat="server" ErrorMessage="Pole wymagane!!!"></asp:RequiredFieldValidator>--%>


    </div>
</div>

<div class="form-group">

    <label class="control-label col-sm-2">Miasto: </label>
    <div class="col-sm-10">
        <asp:DropDownList ID="ddlCity" runat="server"></asp:DropDownList>
         <%--<asp:RequiredFieldValidator ControlToValidate ="ddlCity" ID="RequiredFieldValidator3" runat="server" ErrorMessage="Pole wymagane!!!"></asp:RequiredFieldValidator>--%>

    </div>
</div>

<div class="form-group">

    <label class="control-label col-sm-2">Kraj: </label>
    <div class="col-sm-10">
        <asp:DropDownList ID="ddlCountry" runat="server"></asp:DropDownList>
        <%--<asp:RequiredFieldValidator ControlToValidate ="ddlCountry" ID="RequiredFieldValidator4" runat="server" ErrorMessage="Pole wymagane!!!"></asp:RequiredFieldValidator>--%>

    </div>
</div>
﻿using BiuroPracy.BusinessLogic.Api;
using BiuroPracy.BusinessLogic.Api.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BiuroPracy.Controls
{
    public partial class AddEmployeeControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitControl();
            }
            
        }
        #region Properties
        public string Email => txtEmail.Text;
        public string Password => txtPassword.Text;
        public string Surname => txtSurName.Text;
        public string Name => txtName.Text;
        public int ProfessionId => string.IsNullOrEmpty(ddlProffesion.SelectedValue) ? 0 : Convert.ToInt32(ddlProffesion.SelectedValue);
        public int ContractId => string.IsNullOrEmpty(ddlContractOfEmployment.SelectedValue) ? 0 : Convert.ToInt32(ddlContractOfEmployment.SelectedValue);
        public DateTime DateOfBirth => calendarDateOdBirth.SelectedDate;

        #endregion
        public void ClearControls()
        {
            txtEmail.Text = txtPassword.Text = txtSurName.Text = txtName.Text = string.Empty;
        }
        private void InitControl()
        {
           InitProfession();
            initContract();
        }

        private void initContract()
        {
            IBiuroPracyApi api = new BiuroPracyApi();
            var dataResponse = api.GetContract();
            if (!dataResponse.Success)
            {
                Trace.Warn(dataResponse.Errors);
            }

            ddlContractOfEmployment.DataSource = dataResponse.Data;
            ddlContractOfEmployment.DataTextField = "Name";
            ddlContractOfEmployment.DataValueField = "Id";
            ddlContractOfEmployment.DataBind();
        }

        private void InitProfession()
        {
            IBiuroPracyApi api = new BiuroPracyApi();
            var dataResponse = api.GetProfessions();
            if (!dataResponse.Success)
            {
                Trace.Warn(dataResponse.Errors);
            }

            ddlProffesion.DataSource = dataResponse.Data;
            ddlProffesion.DataTextField = "Name";
            ddlProffesion.DataValueField = "Id";
            ddlProffesion.DataBind();
        }


    }



}
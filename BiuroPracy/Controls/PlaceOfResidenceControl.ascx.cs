﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BiuroPracy.Controls
{
    public partial class PlaceOfResidenceControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region Properties
        public string Street => txtStreet.Text;
        public string PostalCode => txtZipCode.Text;
        public int CityId => string.IsNullOrEmpty(ddlCity.SelectedValue) ? 0 : Convert.ToInt32(ddlCity.SelectedValue);
        public int CountryId => string.IsNullOrEmpty(ddlCountry.SelectedValue) ? 0 : Convert.ToInt32(ddlCountry.SelectedValue);

        #endregion
        public void ClearControls()
        {
            txtStreet.Text = txtZipCode.Text = string.Empty;

        }
    }
}
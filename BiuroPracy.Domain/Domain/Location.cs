﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiuroPracy.Domain.Domain
{
    public class Location
    {
        public virtual int Id { get; set; }
        public virtual string Street { get; set; }
        public virtual City CityID { get; set; }
        public virtual Country CountryID { get; set; }
        public virtual string Postalcode { get; set; }
    }
}
